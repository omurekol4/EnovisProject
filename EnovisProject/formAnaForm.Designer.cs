﻿namespace EnovisProject
{
    partial class formAnaForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formAnaForm));
            this.tabctrlFonksiyonlar = new System.Windows.Forms.TabControl();
            this.tabpageFonksiyon1 = new System.Windows.Forms.TabPage();
            this.btnRastgeleDoldurFonksiyon1 = new System.Windows.Forms.Button();
            this.btnTemizleFonksiyon1 = new System.Windows.Forms.Button();
            this.lblSonucFonksiyon1 = new System.Windows.Forms.Label();
            this.txtSonucFonksiyon1 = new System.Windows.Forms.TextBox();
            this.lblSayi3Fonksiyon1 = new System.Windows.Forms.Label();
            this.lblSayi2Fonksiyon1 = new System.Windows.Forms.Label();
            this.lblSayi1Fonksiyon1 = new System.Windows.Forms.Label();
            this.btnSonucuGosterFonksiyon1 = new System.Windows.Forms.Button();
            this.txtSayi1Fonksiyon1 = new System.Windows.Forms.TextBox();
            this.txtSayi3Fonksiyon1 = new System.Windows.Forms.TextBox();
            this.txtSayi2Fonksiyon1 = new System.Windows.Forms.TextBox();
            this.lblBilgiFonksiyon1 = new System.Windows.Forms.Label();
            this.tabpageFonksiyon2 = new System.Windows.Forms.TabPage();
            this.btnTemizleFonksiyon2 = new System.Windows.Forms.Button();
            this.lblBilgiFonksiyon2 = new System.Windows.Forms.Label();
            this.rchtxtSayilariGosterFonksiyon2 = new System.Windows.Forms.RichTextBox();
            this.btnSayilariGosterFonksiyon2 = new System.Windows.Forms.Button();
            this.lblSayilarFonksiyon2 = new System.Windows.Forms.Label();
            this.tabpageFonksiyon3 = new System.Windows.Forms.TabPage();
            this.btnrastgeleDoldurFonksiyon3 = new System.Windows.Forms.Button();
            this.btnTemizleFonksiyon3 = new System.Windows.Forms.Button();
            this.lblBilgiFonksiyon3 = new System.Windows.Forms.Label();
            this.txtSayiFonksiyon3 = new System.Windows.Forms.TextBox();
            this.rchtxtCarpimTablosuFonksiyon3 = new System.Windows.Forms.RichTextBox();
            this.btnCarpimTablosunuGosterFonksiyon3 = new System.Windows.Forms.Button();
            this.lblSayiFonksiyon3 = new System.Windows.Forms.Label();
            this.tabpageFonksiyon4 = new System.Windows.Forms.TabPage();
            this.lblBilgiFonksiyon4 = new System.Windows.Forms.Label();
            this.btnDosyayiEkleFonksiyon4 = new System.Windows.Forms.Button();
            this.tabpageFonksiyon5 = new System.Windows.Forms.TabPage();
            this.btnRastgeleDoldurFonksiyon5 = new System.Windows.Forms.Button();
            this.btnTemizleFonksiyon5 = new System.Windows.Forms.Button();
            this.btnFibonacciDegeriFonsiyon5 = new System.Windows.Forms.Button();
            this.txbxFibonacciDegeriFonksiyon5 = new System.Windows.Forms.TextBox();
            this.lblFibonacciDegeriFonksiyon5 = new System.Windows.Forms.Label();
            this.lblFibonacciSirasiFonksiyon5 = new System.Windows.Forms.Label();
            this.txbxFibonacciSirasiFonksiyon5 = new System.Windows.Forms.TextBox();
            this.lblBilgiFonksiyon5 = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.pbxAuthor = new System.Windows.Forms.PictureBox();
            this.lblDosyaYoluFonksiyon4 = new System.Windows.Forms.Label();
            this.lblDosyaAdiFonksiyon4 = new System.Windows.Forms.Label();
            this.txtDosyaAdiFonksiyon4 = new System.Windows.Forms.TextBox();
            this.txtDosyaYoluFonksiyon4 = new System.Windows.Forms.TextBox();
            this.rchtxtFonksiyon4 = new System.Windows.Forms.RichTextBox();
            this.btnTemizleFonksiyon4 = new System.Windows.Forms.Button();
            this.tabctrlFonksiyonlar.SuspendLayout();
            this.tabpageFonksiyon1.SuspendLayout();
            this.tabpageFonksiyon2.SuspendLayout();
            this.tabpageFonksiyon3.SuspendLayout();
            this.tabpageFonksiyon4.SuspendLayout();
            this.tabpageFonksiyon5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxAuthor)).BeginInit();
            this.SuspendLayout();
            // 
            // tabctrlFonksiyonlar
            // 
            this.tabctrlFonksiyonlar.Controls.Add(this.tabpageFonksiyon1);
            this.tabctrlFonksiyonlar.Controls.Add(this.tabpageFonksiyon2);
            this.tabctrlFonksiyonlar.Controls.Add(this.tabpageFonksiyon3);
            this.tabctrlFonksiyonlar.Controls.Add(this.tabpageFonksiyon4);
            this.tabctrlFonksiyonlar.Controls.Add(this.tabpageFonksiyon5);
            this.tabctrlFonksiyonlar.Location = new System.Drawing.Point(30, 12);
            this.tabctrlFonksiyonlar.Name = "tabctrlFonksiyonlar";
            this.tabctrlFonksiyonlar.SelectedIndex = 0;
            this.tabctrlFonksiyonlar.Size = new System.Drawing.Size(351, 307);
            this.tabctrlFonksiyonlar.TabIndex = 0;
            // 
            // tabpageFonksiyon1
            // 
            this.tabpageFonksiyon1.Controls.Add(this.btnRastgeleDoldurFonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.btnTemizleFonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.lblSonucFonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.txtSonucFonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.lblSayi3Fonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.lblSayi2Fonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.lblSayi1Fonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.btnSonucuGosterFonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.txtSayi1Fonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.txtSayi3Fonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.txtSayi2Fonksiyon1);
            this.tabpageFonksiyon1.Controls.Add(this.lblBilgiFonksiyon1);
            this.tabpageFonksiyon1.Location = new System.Drawing.Point(4, 22);
            this.tabpageFonksiyon1.Name = "tabpageFonksiyon1";
            this.tabpageFonksiyon1.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageFonksiyon1.Size = new System.Drawing.Size(343, 281);
            this.tabpageFonksiyon1.TabIndex = 0;
            this.tabpageFonksiyon1.Text = "Fonksiyon 1";
            this.tabpageFonksiyon1.UseVisualStyleBackColor = true;
            // 
            // btnRastgeleDoldurFonksiyon1
            // 
            this.btnRastgeleDoldurFonksiyon1.Location = new System.Drawing.Point(212, 24);
            this.btnRastgeleDoldurFonksiyon1.Name = "btnRastgeleDoldurFonksiyon1";
            this.btnRastgeleDoldurFonksiyon1.Size = new System.Drawing.Size(111, 23);
            this.btnRastgeleDoldurFonksiyon1.TabIndex = 17;
            this.btnRastgeleDoldurFonksiyon1.Text = "Rastgele Doldur";
            this.btnRastgeleDoldurFonksiyon1.UseVisualStyleBackColor = true;
            this.btnRastgeleDoldurFonksiyon1.Click += new System.EventHandler(this.btnRastgeleDoldurFonksiyon1_Click);
            // 
            // btnTemizleFonksiyon1
            // 
            this.btnTemizleFonksiyon1.Location = new System.Drawing.Point(15, 130);
            this.btnTemizleFonksiyon1.Name = "btnTemizleFonksiyon1";
            this.btnTemizleFonksiyon1.Size = new System.Drawing.Size(175, 23);
            this.btnTemizleFonksiyon1.TabIndex = 16;
            this.btnTemizleFonksiyon1.Text = "Temizle";
            this.btnTemizleFonksiyon1.UseVisualStyleBackColor = true;
            this.btnTemizleFonksiyon1.Click += new System.EventHandler(this.btnTemizleFonksiyon1_Click);
            // 
            // lblSonucFonksiyon1
            // 
            this.lblSonucFonksiyon1.AutoSize = true;
            this.lblSonucFonksiyon1.Location = new System.Drawing.Point(11, 173);
            this.lblSonucFonksiyon1.Name = "lblSonucFonksiyon1";
            this.lblSonucFonksiyon1.Size = new System.Drawing.Size(129, 13);
            this.lblSonucFonksiyon1.TabIndex = 15;
            this.lblSonucFonksiyon1.Text = "(Say 1 + Sayı 2) * Sayı 3 =";
            // 
            // txtSonucFonksiyon1
            // 
            this.txtSonucFonksiyon1.Location = new System.Drawing.Point(146, 169);
            this.txtSonucFonksiyon1.Name = "txtSonucFonksiyon1";
            this.txtSonucFonksiyon1.ReadOnly = true;
            this.txtSonucFonksiyon1.ShortcutsEnabled = false;
            this.txtSonucFonksiyon1.Size = new System.Drawing.Size(43, 20);
            this.txtSonucFonksiyon1.TabIndex = 14;
            // 
            // lblSayi3Fonksiyon1
            // 
            this.lblSayi3Fonksiyon1.AutoSize = true;
            this.lblSayi3Fonksiyon1.Location = new System.Drawing.Point(12, 81);
            this.lblSayi3Fonksiyon1.Name = "lblSayi3Fonksiyon1";
            this.lblSayi3Fonksiyon1.Size = new System.Drawing.Size(36, 13);
            this.lblSayi3Fonksiyon1.TabIndex = 11;
            this.lblSayi3Fonksiyon1.Text = "Sayı 3";
            // 
            // lblSayi2Fonksiyon1
            // 
            this.lblSayi2Fonksiyon1.AutoSize = true;
            this.lblSayi2Fonksiyon1.Location = new System.Drawing.Point(12, 55);
            this.lblSayi2Fonksiyon1.Name = "lblSayi2Fonksiyon1";
            this.lblSayi2Fonksiyon1.Size = new System.Drawing.Size(36, 13);
            this.lblSayi2Fonksiyon1.TabIndex = 12;
            this.lblSayi2Fonksiyon1.Text = "Sayı 2";
            // 
            // lblSayi1Fonksiyon1
            // 
            this.lblSayi1Fonksiyon1.AutoSize = true;
            this.lblSayi1Fonksiyon1.Location = new System.Drawing.Point(12, 29);
            this.lblSayi1Fonksiyon1.Name = "lblSayi1Fonksiyon1";
            this.lblSayi1Fonksiyon1.Size = new System.Drawing.Size(36, 13);
            this.lblSayi1Fonksiyon1.TabIndex = 13;
            this.lblSayi1Fonksiyon1.Text = "Sayı 1";
            // 
            // btnSonucuGosterFonksiyon1
            // 
            this.btnSonucuGosterFonksiyon1.Location = new System.Drawing.Point(15, 111);
            this.btnSonucuGosterFonksiyon1.Name = "btnSonucuGosterFonksiyon1";
            this.btnSonucuGosterFonksiyon1.Size = new System.Drawing.Size(175, 23);
            this.btnSonucuGosterFonksiyon1.TabIndex = 10;
            this.btnSonucuGosterFonksiyon1.Text = "Sonucu Göster";
            this.btnSonucuGosterFonksiyon1.UseVisualStyleBackColor = true;
            this.btnSonucuGosterFonksiyon1.Click += new System.EventHandler(this.btnSonucuGosterFonksiyon1_Click);
            // 
            // txtSayi1Fonksiyon1
            // 
            this.txtSayi1Fonksiyon1.Location = new System.Drawing.Point(75, 26);
            this.txtSayi1Fonksiyon1.Name = "txtSayi1Fonksiyon1";
            this.txtSayi1Fonksiyon1.Size = new System.Drawing.Size(114, 20);
            this.txtSayi1Fonksiyon1.TabIndex = 7;
            // 
            // txtSayi3Fonksiyon1
            // 
            this.txtSayi3Fonksiyon1.Location = new System.Drawing.Point(75, 78);
            this.txtSayi3Fonksiyon1.Name = "txtSayi3Fonksiyon1";
            this.txtSayi3Fonksiyon1.Size = new System.Drawing.Size(115, 20);
            this.txtSayi3Fonksiyon1.TabIndex = 8;
            // 
            // txtSayi2Fonksiyon1
            // 
            this.txtSayi2Fonksiyon1.Location = new System.Drawing.Point(75, 52);
            this.txtSayi2Fonksiyon1.Name = "txtSayi2Fonksiyon1";
            this.txtSayi2Fonksiyon1.Size = new System.Drawing.Size(114, 20);
            this.txtSayi2Fonksiyon1.TabIndex = 9;
            // 
            // lblBilgiFonksiyon1
            // 
            this.lblBilgiFonksiyon1.AutoSize = true;
            this.lblBilgiFonksiyon1.Location = new System.Drawing.Point(6, 7);
            this.lblBilgiFonksiyon1.Name = "lblBilgiFonksiyon1";
            this.lblBilgiFonksiyon1.Size = new System.Drawing.Size(100, 13);
            this.lblBilgiFonksiyon1.TabIndex = 6;
            this.lblBilgiFonksiyon1.Text = "*Lütfen 3 sayı giriniz";
            // 
            // tabpageFonksiyon2
            // 
            this.tabpageFonksiyon2.Controls.Add(this.btnTemizleFonksiyon2);
            this.tabpageFonksiyon2.Controls.Add(this.lblBilgiFonksiyon2);
            this.tabpageFonksiyon2.Controls.Add(this.rchtxtSayilariGosterFonksiyon2);
            this.tabpageFonksiyon2.Controls.Add(this.btnSayilariGosterFonksiyon2);
            this.tabpageFonksiyon2.Controls.Add(this.lblSayilarFonksiyon2);
            this.tabpageFonksiyon2.Location = new System.Drawing.Point(4, 22);
            this.tabpageFonksiyon2.Name = "tabpageFonksiyon2";
            this.tabpageFonksiyon2.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageFonksiyon2.Size = new System.Drawing.Size(343, 281);
            this.tabpageFonksiyon2.TabIndex = 1;
            this.tabpageFonksiyon2.Text = "Fonksiyon 2";
            this.tabpageFonksiyon2.UseVisualStyleBackColor = true;
            // 
            // btnTemizleFonksiyon2
            // 
            this.btnTemizleFonksiyon2.Location = new System.Drawing.Point(100, 166);
            this.btnTemizleFonksiyon2.Name = "btnTemizleFonksiyon2";
            this.btnTemizleFonksiyon2.Size = new System.Drawing.Size(226, 23);
            this.btnTemizleFonksiyon2.TabIndex = 14;
            this.btnTemizleFonksiyon2.Text = "Temizle";
            this.btnTemizleFonksiyon2.UseVisualStyleBackColor = true;
            this.btnTemizleFonksiyon2.Click += new System.EventHandler(this.btnTemizleFonksiyon2_Click);
            // 
            // lblBilgiFonksiyon2
            // 
            this.lblBilgiFonksiyon2.AutoSize = true;
            this.lblBilgiFonksiyon2.Location = new System.Drawing.Point(2, 3);
            this.lblBilgiFonksiyon2.Name = "lblBilgiFonksiyon2";
            this.lblBilgiFonksiyon2.Size = new System.Drawing.Size(214, 13);
            this.lblBilgiFonksiyon2.TabIndex = 13;
            this.lblBilgiFonksiyon2.Text = "*Lütfen \"Sayıları Göster\" butonuna tıklayınız.";
            // 
            // rchtxtSayilariGosterFonksiyon2
            // 
            this.rchtxtSayilariGosterFonksiyon2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.rchtxtSayilariGosterFonksiyon2.Location = new System.Drawing.Point(100, 25);
            this.rchtxtSayilariGosterFonksiyon2.Name = "rchtxtSayilariGosterFonksiyon2";
            this.rchtxtSayilariGosterFonksiyon2.ReadOnly = true;
            this.rchtxtSayilariGosterFonksiyon2.Size = new System.Drawing.Size(226, 116);
            this.rchtxtSayilariGosterFonksiyon2.TabIndex = 12;
            this.rchtxtSayilariGosterFonksiyon2.Text = "";
            // 
            // btnSayilariGosterFonksiyon2
            // 
            this.btnSayilariGosterFonksiyon2.Location = new System.Drawing.Point(100, 147);
            this.btnSayilariGosterFonksiyon2.Name = "btnSayilariGosterFonksiyon2";
            this.btnSayilariGosterFonksiyon2.Size = new System.Drawing.Size(226, 23);
            this.btnSayilariGosterFonksiyon2.TabIndex = 11;
            this.btnSayilariGosterFonksiyon2.Text = "Sayıları Göster";
            this.btnSayilariGosterFonksiyon2.UseVisualStyleBackColor = true;
            this.btnSayilariGosterFonksiyon2.Click += new System.EventHandler(this.btnSayilariGosterFonksiyon2_Click);
            // 
            // lblSayilarFonksiyon2
            // 
            this.lblSayilarFonksiyon2.AutoSize = true;
            this.lblSayilarFonksiyon2.Location = new System.Drawing.Point(2, 25);
            this.lblSayilarFonksiyon2.Name = "lblSayilarFonksiyon2";
            this.lblSayilarFonksiyon2.Size = new System.Drawing.Size(89, 13);
            this.lblSayilarFonksiyon2.TabIndex = 10;
            this.lblSayilarFonksiyon2.Text = "Sayılar (1-200)  = ";
            // 
            // tabpageFonksiyon3
            // 
            this.tabpageFonksiyon3.Controls.Add(this.btnrastgeleDoldurFonksiyon3);
            this.tabpageFonksiyon3.Controls.Add(this.btnTemizleFonksiyon3);
            this.tabpageFonksiyon3.Controls.Add(this.lblBilgiFonksiyon3);
            this.tabpageFonksiyon3.Controls.Add(this.txtSayiFonksiyon3);
            this.tabpageFonksiyon3.Controls.Add(this.rchtxtCarpimTablosuFonksiyon3);
            this.tabpageFonksiyon3.Controls.Add(this.btnCarpimTablosunuGosterFonksiyon3);
            this.tabpageFonksiyon3.Controls.Add(this.lblSayiFonksiyon3);
            this.tabpageFonksiyon3.Location = new System.Drawing.Point(4, 22);
            this.tabpageFonksiyon3.Name = "tabpageFonksiyon3";
            this.tabpageFonksiyon3.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageFonksiyon3.Size = new System.Drawing.Size(343, 281);
            this.tabpageFonksiyon3.TabIndex = 2;
            this.tabpageFonksiyon3.Text = "Fonksiyon 3";
            this.tabpageFonksiyon3.UseVisualStyleBackColor = true;
            // 
            // btnrastgeleDoldurFonksiyon3
            // 
            this.btnrastgeleDoldurFonksiyon3.Location = new System.Drawing.Point(226, 16);
            this.btnrastgeleDoldurFonksiyon3.Name = "btnrastgeleDoldurFonksiyon3";
            this.btnrastgeleDoldurFonksiyon3.Size = new System.Drawing.Size(111, 23);
            this.btnrastgeleDoldurFonksiyon3.TabIndex = 15;
            this.btnrastgeleDoldurFonksiyon3.Text = "Rastgele Doldur";
            this.btnrastgeleDoldurFonksiyon3.UseVisualStyleBackColor = true;
            this.btnrastgeleDoldurFonksiyon3.Click += new System.EventHandler(this.btnrastgeleDoldurFonksiyon3_Click);
            // 
            // btnTemizleFonksiyon3
            // 
            this.btnTemizleFonksiyon3.Location = new System.Drawing.Point(6, 65);
            this.btnTemizleFonksiyon3.Name = "btnTemizleFonksiyon3";
            this.btnTemizleFonksiyon3.Size = new System.Drawing.Size(331, 23);
            this.btnTemizleFonksiyon3.TabIndex = 14;
            this.btnTemizleFonksiyon3.Text = "Temizle";
            this.btnTemizleFonksiyon3.UseVisualStyleBackColor = true;
            this.btnTemizleFonksiyon3.Click += new System.EventHandler(this.btnTemizleFonksiyon3_Click);
            // 
            // lblBilgiFonksiyon3
            // 
            this.lblBilgiFonksiyon3.AutoSize = true;
            this.lblBilgiFonksiyon3.Location = new System.Drawing.Point(0, 2);
            this.lblBilgiFonksiyon3.Name = "lblBilgiFonksiyon3";
            this.lblBilgiFonksiyon3.Size = new System.Drawing.Size(205, 13);
            this.lblBilgiFonksiyon3.TabIndex = 13;
            this.lblBilgiFonksiyon3.Text = "*Lütfen 1 ile 15 arasında bir tamsayı giriniz ";
            // 
            // txtSayiFonksiyon3
            // 
            this.txtSayiFonksiyon3.Location = new System.Drawing.Point(95, 18);
            this.txtSayiFonksiyon3.Name = "txtSayiFonksiyon3";
            this.txtSayiFonksiyon3.Size = new System.Drawing.Size(125, 20);
            this.txtSayiFonksiyon3.TabIndex = 12;
            // 
            // rchtxtCarpimTablosuFonksiyon3
            // 
            this.rchtxtCarpimTablosuFonksiyon3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.rchtxtCarpimTablosuFonksiyon3.Location = new System.Drawing.Point(6, 94);
            this.rchtxtCarpimTablosuFonksiyon3.Name = "rchtxtCarpimTablosuFonksiyon3";
            this.rchtxtCarpimTablosuFonksiyon3.ReadOnly = true;
            this.rchtxtCarpimTablosuFonksiyon3.Size = new System.Drawing.Size(331, 181);
            this.rchtxtCarpimTablosuFonksiyon3.TabIndex = 11;
            this.rchtxtCarpimTablosuFonksiyon3.Text = "";
            // 
            // btnCarpimTablosunuGosterFonksiyon3
            // 
            this.btnCarpimTablosunuGosterFonksiyon3.Location = new System.Drawing.Point(6, 44);
            this.btnCarpimTablosunuGosterFonksiyon3.Name = "btnCarpimTablosunuGosterFonksiyon3";
            this.btnCarpimTablosunuGosterFonksiyon3.Size = new System.Drawing.Size(331, 23);
            this.btnCarpimTablosunuGosterFonksiyon3.TabIndex = 10;
            this.btnCarpimTablosunuGosterFonksiyon3.Text = "Çarpım Tablosunu Göster";
            this.btnCarpimTablosunuGosterFonksiyon3.UseVisualStyleBackColor = true;
            this.btnCarpimTablosunuGosterFonksiyon3.Click += new System.EventHandler(this.btnCarpimTablosunuGosterFonksiyon3_Click);
            // 
            // lblSayiFonksiyon3
            // 
            this.lblSayiFonksiyon3.AutoSize = true;
            this.lblSayiFonksiyon3.Location = new System.Drawing.Point(0, 21);
            this.lblSayiFonksiyon3.Name = "lblSayiFonksiyon3";
            this.lblSayiFonksiyon3.Size = new System.Drawing.Size(72, 13);
            this.lblSayiFonksiyon3.TabIndex = 9;
            this.lblSayiFonksiyon3.Text = "Sayı (1-15)  = ";
            // 
            // tabpageFonksiyon4
            // 
            this.tabpageFonksiyon4.Controls.Add(this.btnTemizleFonksiyon4);
            this.tabpageFonksiyon4.Controls.Add(this.rchtxtFonksiyon4);
            this.tabpageFonksiyon4.Controls.Add(this.txtDosyaYoluFonksiyon4);
            this.tabpageFonksiyon4.Controls.Add(this.txtDosyaAdiFonksiyon4);
            this.tabpageFonksiyon4.Controls.Add(this.lblDosyaAdiFonksiyon4);
            this.tabpageFonksiyon4.Controls.Add(this.lblDosyaYoluFonksiyon4);
            this.tabpageFonksiyon4.Controls.Add(this.lblBilgiFonksiyon4);
            this.tabpageFonksiyon4.Controls.Add(this.btnDosyayiEkleFonksiyon4);
            this.tabpageFonksiyon4.Location = new System.Drawing.Point(4, 22);
            this.tabpageFonksiyon4.Name = "tabpageFonksiyon4";
            this.tabpageFonksiyon4.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageFonksiyon4.Size = new System.Drawing.Size(343, 281);
            this.tabpageFonksiyon4.TabIndex = 3;
            this.tabpageFonksiyon4.Text = "Fonksiyon 4";
            this.tabpageFonksiyon4.UseVisualStyleBackColor = true;
            // 
            // lblBilgiFonksiyon4
            // 
            this.lblBilgiFonksiyon4.AutoSize = true;
            this.lblBilgiFonksiyon4.Location = new System.Drawing.Point(3, 6);
            this.lblBilgiFonksiyon4.Name = "lblBilgiFonksiyon4";
            this.lblBilgiFonksiyon4.Size = new System.Drawing.Size(160, 13);
            this.lblBilgiFonksiyon4.TabIndex = 14;
            this.lblBilgiFonksiyon4.Text = "*Lütfen Bir Text Dosyası Seçiniz.";
            // 
            // btnDosyayiEkleFonksiyon4
            // 
            this.btnDosyayiEkleFonksiyon4.Location = new System.Drawing.Point(6, 51);
            this.btnDosyayiEkleFonksiyon4.Name = "btnDosyayiEkleFonksiyon4";
            this.btnDosyayiEkleFonksiyon4.Size = new System.Drawing.Size(120, 23);
            this.btnDosyayiEkleFonksiyon4.TabIndex = 8;
            this.btnDosyayiEkleFonksiyon4.Text = "Dosyayı Ekle";
            this.btnDosyayiEkleFonksiyon4.UseVisualStyleBackColor = true;
            this.btnDosyayiEkleFonksiyon4.Click += new System.EventHandler(this.btnDosyayiEkleFonksiyon4_Click);
            // 
            // tabpageFonksiyon5
            // 
            this.tabpageFonksiyon5.Controls.Add(this.btnRastgeleDoldurFonksiyon5);
            this.tabpageFonksiyon5.Controls.Add(this.btnTemizleFonksiyon5);
            this.tabpageFonksiyon5.Controls.Add(this.btnFibonacciDegeriFonsiyon5);
            this.tabpageFonksiyon5.Controls.Add(this.txbxFibonacciDegeriFonksiyon5);
            this.tabpageFonksiyon5.Controls.Add(this.lblFibonacciDegeriFonksiyon5);
            this.tabpageFonksiyon5.Controls.Add(this.lblFibonacciSirasiFonksiyon5);
            this.tabpageFonksiyon5.Controls.Add(this.txbxFibonacciSirasiFonksiyon5);
            this.tabpageFonksiyon5.Controls.Add(this.lblBilgiFonksiyon5);
            this.tabpageFonksiyon5.Location = new System.Drawing.Point(4, 22);
            this.tabpageFonksiyon5.Name = "tabpageFonksiyon5";
            this.tabpageFonksiyon5.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageFonksiyon5.Size = new System.Drawing.Size(343, 281);
            this.tabpageFonksiyon5.TabIndex = 4;
            this.tabpageFonksiyon5.Text = "Fonksiyon 5";
            this.tabpageFonksiyon5.UseVisualStyleBackColor = true;
            // 
            // btnRastgeleDoldurFonksiyon5
            // 
            this.btnRastgeleDoldurFonksiyon5.Location = new System.Drawing.Point(226, 45);
            this.btnRastgeleDoldurFonksiyon5.Name = "btnRastgeleDoldurFonksiyon5";
            this.btnRastgeleDoldurFonksiyon5.Size = new System.Drawing.Size(111, 23);
            this.btnRastgeleDoldurFonksiyon5.TabIndex = 22;
            this.btnRastgeleDoldurFonksiyon5.Text = "Rastgele Doldur";
            this.btnRastgeleDoldurFonksiyon5.UseVisualStyleBackColor = true;
            this.btnRastgeleDoldurFonksiyon5.Click += new System.EventHandler(this.btnRastgeleDoldurFonksiyon5_Click);
            // 
            // btnTemizleFonksiyon5
            // 
            this.btnTemizleFonksiyon5.Location = new System.Drawing.Point(22, 96);
            this.btnTemizleFonksiyon5.Name = "btnTemizleFonksiyon5";
            this.btnTemizleFonksiyon5.Size = new System.Drawing.Size(180, 23);
            this.btnTemizleFonksiyon5.TabIndex = 21;
            this.btnTemizleFonksiyon5.Text = "Temizle";
            this.btnTemizleFonksiyon5.UseVisualStyleBackColor = true;
            this.btnTemizleFonksiyon5.Click += new System.EventHandler(this.btnTemizleFonksiyon5_Click);
            // 
            // btnFibonacciDegeriFonsiyon5
            // 
            this.btnFibonacciDegeriFonsiyon5.Location = new System.Drawing.Point(22, 71);
            this.btnFibonacciDegeriFonsiyon5.Name = "btnFibonacciDegeriFonsiyon5";
            this.btnFibonacciDegeriFonsiyon5.Size = new System.Drawing.Size(180, 23);
            this.btnFibonacciDegeriFonsiyon5.TabIndex = 20;
            this.btnFibonacciDegeriFonsiyon5.Text = "Fibonacci Degerini Göster";
            this.btnFibonacciDegeriFonsiyon5.UseVisualStyleBackColor = true;
            this.btnFibonacciDegeriFonsiyon5.Click += new System.EventHandler(this.btnFibonacciDegeriFonsiyon5_Click);
            // 
            // txbxFibonacciDegeriFonksiyon5
            // 
            this.txbxFibonacciDegeriFonksiyon5.Location = new System.Drawing.Point(118, 130);
            this.txbxFibonacciDegeriFonksiyon5.Name = "txbxFibonacciDegeriFonksiyon5";
            this.txbxFibonacciDegeriFonksiyon5.ReadOnly = true;
            this.txbxFibonacciDegeriFonksiyon5.Size = new System.Drawing.Size(84, 20);
            this.txbxFibonacciDegeriFonksiyon5.TabIndex = 19;
            // 
            // lblFibonacciDegeriFonksiyon5
            // 
            this.lblFibonacciDegeriFonksiyon5.AutoSize = true;
            this.lblFibonacciDegeriFonksiyon5.Location = new System.Drawing.Point(19, 133);
            this.lblFibonacciDegeriFonksiyon5.Name = "lblFibonacciDegeriFonksiyon5";
            this.lblFibonacciDegeriFonksiyon5.Size = new System.Drawing.Size(93, 13);
            this.lblFibonacciDegeriFonksiyon5.TabIndex = 18;
            this.lblFibonacciDegeriFonksiyon5.Text = "Fibonacci Değeri :";
            // 
            // lblFibonacciSirasiFonksiyon5
            // 
            this.lblFibonacciSirasiFonksiyon5.AutoSize = true;
            this.lblFibonacciSirasiFonksiyon5.Location = new System.Drawing.Point(19, 48);
            this.lblFibonacciSirasiFonksiyon5.Name = "lblFibonacciSirasiFonksiyon5";
            this.lblFibonacciSirasiFonksiyon5.Size = new System.Drawing.Size(87, 13);
            this.lblFibonacciSirasiFonksiyon5.TabIndex = 17;
            this.lblFibonacciSirasiFonksiyon5.Text = "Fibonacci Sırası :";
            // 
            // txbxFibonacciSirasiFonksiyon5
            // 
            this.txbxFibonacciSirasiFonksiyon5.Location = new System.Drawing.Point(118, 45);
            this.txbxFibonacciSirasiFonksiyon5.Name = "txbxFibonacciSirasiFonksiyon5";
            this.txbxFibonacciSirasiFonksiyon5.Size = new System.Drawing.Size(84, 20);
            this.txbxFibonacciSirasiFonksiyon5.TabIndex = 16;
            // 
            // lblBilgiFonksiyon5
            // 
            this.lblBilgiFonksiyon5.AutoSize = true;
            this.lblBilgiFonksiyon5.Location = new System.Drawing.Point(-1, 3);
            this.lblBilgiFonksiyon5.Name = "lblBilgiFonksiyon5";
            this.lblBilgiFonksiyon5.Size = new System.Drawing.Size(341, 13);
            this.lblBilgiFonksiyon5.TabIndex = 15;
            this.lblBilgiFonksiyon5.Text = "*Lütfen fibonacci değerini öğrenmek istediğiniz bir sıra numarası giriniz : ";
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.Location = new System.Drawing.Point(131, 322);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(105, 13);
            this.lblAuthor.TabIndex = 7;
            this.lblAuthor.Text = "coded by Ömür Alçin";
            // 
            // pbxAuthor
            // 
            this.pbxAuthor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbxAuthor.Image = ((System.Drawing.Image)(resources.GetObject("pbxAuthor.Image")));
            this.pbxAuthor.Location = new System.Drawing.Point(242, 320);
            this.pbxAuthor.Name = "pbxAuthor";
            this.pbxAuthor.Size = new System.Drawing.Size(25, 22);
            this.pbxAuthor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxAuthor.TabIndex = 8;
            this.pbxAuthor.TabStop = false;
            // 
            // lblDosyaYoluFonksiyon4
            // 
            this.lblDosyaYoluFonksiyon4.AutoSize = true;
            this.lblDosyaYoluFonksiyon4.Location = new System.Drawing.Point(3, 109);
            this.lblDosyaYoluFonksiyon4.Name = "lblDosyaYoluFonksiyon4";
            this.lblDosyaYoluFonksiyon4.Size = new System.Drawing.Size(67, 13);
            this.lblDosyaYoluFonksiyon4.TabIndex = 15;
            this.lblDosyaYoluFonksiyon4.Text = "Dosya Yolu :";
            // 
            // lblDosyaAdiFonksiyon4
            // 
            this.lblDosyaAdiFonksiyon4.AutoSize = true;
            this.lblDosyaAdiFonksiyon4.Location = new System.Drawing.Point(3, 83);
            this.lblDosyaAdiFonksiyon4.Name = "lblDosyaAdiFonksiyon4";
            this.lblDosyaAdiFonksiyon4.Size = new System.Drawing.Size(67, 13);
            this.lblDosyaAdiFonksiyon4.TabIndex = 16;
            this.lblDosyaAdiFonksiyon4.Text = "Dosya Adı   :";
            // 
            // txtDosyaAdiFonksiyon4
            // 
            this.txtDosyaAdiFonksiyon4.Location = new System.Drawing.Point(73, 80);
            this.txtDosyaAdiFonksiyon4.Name = "txtDosyaAdiFonksiyon4";
            this.txtDosyaAdiFonksiyon4.Size = new System.Drawing.Size(243, 20);
            this.txtDosyaAdiFonksiyon4.TabIndex = 17;
            // 
            // txtDosyaYoluFonksiyon4
            // 
            this.txtDosyaYoluFonksiyon4.Location = new System.Drawing.Point(73, 106);
            this.txtDosyaYoluFonksiyon4.Name = "txtDosyaYoluFonksiyon4";
            this.txtDosyaYoluFonksiyon4.Size = new System.Drawing.Size(243, 20);
            this.txtDosyaYoluFonksiyon4.TabIndex = 18;
            // 
            // rchtxtFonksiyon4
            // 
            this.rchtxtFonksiyon4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.rchtxtFonksiyon4.Location = new System.Drawing.Point(6, 132);
            this.rchtxtFonksiyon4.Name = "rchtxtFonksiyon4";
            this.rchtxtFonksiyon4.Size = new System.Drawing.Size(331, 143);
            this.rchtxtFonksiyon4.TabIndex = 19;
            this.rchtxtFonksiyon4.Text = "";
            // 
            // btnTemizleFonksiyon4
            // 
            this.btnTemizleFonksiyon4.Location = new System.Drawing.Point(241, 51);
            this.btnTemizleFonksiyon4.Name = "btnTemizleFonksiyon4";
            this.btnTemizleFonksiyon4.Size = new System.Drawing.Size(75, 23);
            this.btnTemizleFonksiyon4.TabIndex = 20;
            this.btnTemizleFonksiyon4.Text = "Temizle";
            this.btnTemizleFonksiyon4.UseVisualStyleBackColor = true;
            this.btnTemizleFonksiyon4.Click += new System.EventHandler(this.btnTemizleFonksiyon4_Click);
            // 
            // formAnaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 354);
            this.Controls.Add(this.pbxAuthor);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.tabctrlFonksiyonlar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "formAnaForm";
            this.Text = "EnovisProject v.0.1";
            this.tabctrlFonksiyonlar.ResumeLayout(false);
            this.tabpageFonksiyon1.ResumeLayout(false);
            this.tabpageFonksiyon1.PerformLayout();
            this.tabpageFonksiyon2.ResumeLayout(false);
            this.tabpageFonksiyon2.PerformLayout();
            this.tabpageFonksiyon3.ResumeLayout(false);
            this.tabpageFonksiyon3.PerformLayout();
            this.tabpageFonksiyon4.ResumeLayout(false);
            this.tabpageFonksiyon4.PerformLayout();
            this.tabpageFonksiyon5.ResumeLayout(false);
            this.tabpageFonksiyon5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxAuthor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabctrlFonksiyonlar;
        private System.Windows.Forms.TabPage tabpageFonksiyon1;
        internal System.Windows.Forms.Label lblSonucFonksiyon1;
        internal System.Windows.Forms.TextBox txtSonucFonksiyon1;
        internal System.Windows.Forms.Label lblSayi3Fonksiyon1;
        internal System.Windows.Forms.Label lblSayi2Fonksiyon1;
        internal System.Windows.Forms.Label lblSayi1Fonksiyon1;
        internal System.Windows.Forms.Button btnSonucuGosterFonksiyon1;
        internal System.Windows.Forms.TextBox txtSayi1Fonksiyon1;
        internal System.Windows.Forms.TextBox txtSayi3Fonksiyon1;
        internal System.Windows.Forms.TextBox txtSayi2Fonksiyon1;
        internal System.Windows.Forms.Label lblBilgiFonksiyon1;
        private System.Windows.Forms.TabPage tabpageFonksiyon2;
        internal System.Windows.Forms.Label lblBilgiFonksiyon2;
        internal System.Windows.Forms.RichTextBox rchtxtSayilariGosterFonksiyon2;
        internal System.Windows.Forms.Button btnSayilariGosterFonksiyon2;
        internal System.Windows.Forms.Label lblSayilarFonksiyon2;
        private System.Windows.Forms.TabPage tabpageFonksiyon3;
        internal System.Windows.Forms.Label lblBilgiFonksiyon3;
        internal System.Windows.Forms.TextBox txtSayiFonksiyon3;
        internal System.Windows.Forms.RichTextBox rchtxtCarpimTablosuFonksiyon3;
        internal System.Windows.Forms.Button btnCarpimTablosunuGosterFonksiyon3;
        internal System.Windows.Forms.Label lblSayiFonksiyon3;
        private System.Windows.Forms.TabPage tabpageFonksiyon4;
        internal System.Windows.Forms.Label lblBilgiFonksiyon4;
        internal System.Windows.Forms.Button btnDosyayiEkleFonksiyon4;
        private System.Windows.Forms.TabPage tabpageFonksiyon5;
        internal System.Windows.Forms.TextBox txbxFibonacciDegeriFonksiyon5;
        internal System.Windows.Forms.Label lblFibonacciDegeriFonksiyon5;
        internal System.Windows.Forms.Label lblFibonacciSirasiFonksiyon5;
        internal System.Windows.Forms.TextBox txbxFibonacciSirasiFonksiyon5;
        internal System.Windows.Forms.Label lblBilgiFonksiyon5;
        internal System.Windows.Forms.Label lblAuthor;
        internal System.Windows.Forms.Button btnRastgeleDoldurFonksiyon1;
        internal System.Windows.Forms.Button btnTemizleFonksiyon1;
        internal System.Windows.Forms.Button btnTemizleFonksiyon2;
        internal System.Windows.Forms.Button btnTemizleFonksiyon3;
        private System.Windows.Forms.PictureBox pbxAuthor;
        private System.Windows.Forms.Button btnFibonacciDegeriFonsiyon5;
        private System.Windows.Forms.Button btnTemizleFonksiyon5;
        private System.Windows.Forms.Button btnRastgeleDoldurFonksiyon5;
        private System.Windows.Forms.Button btnrastgeleDoldurFonksiyon3;
        private System.Windows.Forms.TextBox txtDosyaYoluFonksiyon4;
        private System.Windows.Forms.TextBox txtDosyaAdiFonksiyon4;
        private System.Windows.Forms.Label lblDosyaAdiFonksiyon4;
        private System.Windows.Forms.Label lblDosyaYoluFonksiyon4;
        private System.Windows.Forms.RichTextBox rchtxtFonksiyon4;
        private System.Windows.Forms.Button btnTemizleFonksiyon4;
    }
}

