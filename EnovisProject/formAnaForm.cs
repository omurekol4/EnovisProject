﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EnovisProject
{
    public partial class formAnaForm : Form
    {
        public formAnaForm()
        {
            InitializeComponent();
        }

        //Fonksiyon1
        // Girilen 3 sayıyı aşağıdaki formule göre çıkan sonucunu gösterir :
        // -> (sayi1 + sayi2) * sayi3  
        #region Fonksiyon1
        private void btnRastgeleDoldurFonksiyon1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            txtSayi1Fonksiyon1.Text = rnd.Next(1, 100).ToString();
            txtSayi2Fonksiyon1.Text = rnd.Next(1, 100).ToString();
            txtSayi3Fonksiyon1.Text = rnd.Next(1, 100).ToString();
        }
        private void btnSonucuGosterFonksiyon1_Click(object sender, EventArgs e)
        {
            //Degiskenler tanımlandı
            int sayi1 = 0, sayi2 = 0, sayi3 = 0, toplam = 0;
            try
            {
                //formdan gelen değerler değişkenlere atandı
                sayi1 = Convert.ToInt32(txtSayi1Fonksiyon1.Text);
                sayi2 = Convert.ToInt32(txtSayi2Fonksiyon1.Text);
                sayi3 = Convert.ToInt32(txtSayi3Fonksiyon1.Text);
                // gelen değerler işlenip sonuç forma basıldı
                toplam = (sayi1 + sayi2) * sayi3;
                txtSonucFonksiyon1.Text = toplam.ToString();

            }
            catch (Exception)
            {
                MessageBox.Show("Hatalı Veri!", "Lütfen sayı giriniz !", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void btnTemizleFonksiyon1_Click(object sender, EventArgs e)
        {
            txtSayi1Fonksiyon1.Text = "";
            txtSayi2Fonksiyon1.Text = "";
            txtSayi3Fonksiyon1.Text = "";
            txtSonucFonksiyon1.Text = "";
        }
        #endregion

        //Fonksiyon2 
        // 1 ile 200 arasındaki sayıları aşağıdaki şartlara göre gösterir
        // -> 100 den küçük olup 5 ve 3 ün katı olanları "zigzag". 
        // -> 100 den büyük olup 5 ve 3 ün katı olanları "zagzig". 
        // -> 3 ün katı olanları "zig"
        // -> 5 in katı olanları "zag"
        #region Fonksiyon2
        private void btnSayilariGosterFonksiyon2_Click(object sender, EventArgs e)
        {
            string[] sayilar = new string[201];
            for (int i = 1; i <= 200; i++)
            {
                sayilar[i] = i.ToString();
                if (i % 3 == 0 && i % 5 == 0)
                {
                    if (i <= 100)
                        sayilar[i] = "zigzag";
                    else
                        sayilar[i] = "zagzig";
                }
                else if (i % 3 == 0)
                {
                    sayilar[i] = "zig";
                }
                else if (i % 5 == 0)
                {
                    sayilar[i] = "zag";
                }
                rchtxtSayilariGosterFonksiyon2.AppendText(sayilar[i].ToString() + " | ");
            }
        }
        private void btnTemizleFonksiyon2_Click(object sender, EventArgs e)
        {
            rchtxtSayilariGosterFonksiyon2.Text = "";
        }
        #endregion   

        //Fonksiyon3
        // 1 ile 15 arasında girilen bir sayının çarpım tablosunu gösterir. 
        #region Fonksiyon3
        private void btnrastgeleDoldurFonksiyon3_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            txtSayiFonksiyon3.Text = "" + rnd.Next(1, 15);
        }
        private void btnCarpimTablosunuGosterFonksiyon3_Click(object sender, EventArgs e)
        {
            int sayi;
            try
            {
                sayi = Convert.ToInt32(txtSayiFonksiyon3.Text);
                if (sayi >= 1 && sayi <= 15)
                {
                    int[,] matris = new int[sayi, sayi];
                    rchtxtCarpimTablosuFonksiyon3.AppendText("--------1 ile " + sayi + " arası çarpim tablosu-----------------\n");
                    for (int i = 1; i <= sayi; i++)
                    {
                        rchtxtCarpimTablosuFonksiyon3.AppendText(i + " ");
                        for (int j = 1; j <= sayi; j++)
                        {
                            rchtxtCarpimTablosuFonksiyon3.AppendText(i * j + " ");
                        }
                        rchtxtCarpimTablosuFonksiyon3.AppendText("\n");
                    }
                    rchtxtCarpimTablosuFonksiyon3.AppendText("-----------------------------------------------\n");
                }
                else
                {
                    MessageBox.Show("Sayi 1- ile 15 arasında değil !", "Sınır", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Hatalı Veri!", "Lütfen sayı giriniz !", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnTemizleFonksiyon3_Click(object sender, EventArgs e)
        {
            rchtxtCarpimTablosuFonksiyon3.Text = "";
            txtSayiFonksiyon3.Text = "";
        }
        #endregion//Fonksiyon3-1 ile 15 arasında girilen bir sayının çarpım tablosunu gösterir.

        //Fonksiyon4
        //Bilgisayardan seçilen metin dosyasındaki değerleri parse eder.
        #region Fonksiyon4
        private void btnDosyayiEkleFonksiyon4_Click(object sender, EventArgs e)
        {
            OpenFileDialog dosya = new OpenFileDialog();
            dosya.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dosya.Filter = "Text Dosyası |*.txt";
            dosya.RestoreDirectory = true;
            dosya.Title = "Text Dosyası Seçiniz..";


            if (dosya.ShowDialog() == DialogResult.OK)
            {
                txtDosyaYoluFonksiyon4.Text = dosya.FileName;
                txtDosyaAdiFonksiyon4.Text = dosya.SafeFileName;
                StreamReader oku = File.OpenText("" + dosya.FileName);
                decimal[] sayilar = new decimal[200];
                string satir = "";
                string[] dizi;

                int i = 0;
                while ((satir = oku.ReadLine()) != null)
                {
                    dizi = satir.Split(' ');
                    for (int k = 0; k < dizi.Length; k++)
                    {
                        if (!string.IsNullOrEmpty(dizi[k]))
                        {
                            sayilar[i] = Convert.ToDecimal(dizi[k]);

                            i++;
                        }
                    }

                }
                decimal temp = 0;
                decimal[] buyuk = new decimal[200];
                for (int s = 0; s < i; s++)
                {
                    //rchtxtFonksiyon4.AppendText(s + 1 + "-)" + sayilar[s] + "\n");


                    for (int t = 0; t < i - s; t++)
                    {

                        if (sayilar[s] < sayilar[s + t])
                        {
                            temp = sayilar[s];
                            sayilar[s] = sayilar[s + t];
                            sayilar[s + t] = temp;
                        }
                    } 
                }
                for (int z = 0; z < i; z++)
                {
                    rchtxtFonksiyon4.AppendText(z + 1 + "-)" + sayilar[z] + "\n");
                } 
            }

        }
        private void btnTemizleFonksiyon4_Click(object sender, EventArgs e)
        {
            txtDosyaYoluFonksiyon4.Text = "";
            txtDosyaAdiFonksiyon4.Text = "";
            rchtxtFonksiyon4.Text = "";
        }
        #endregion
       
        //Fonksiyon5
        // Girilen Fibonacci dizi numarasının Fibonacci değerini gösterir. 
        #region Fonksiyon5
        private void btnRastgeleDoldurFonksiyon5_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            txbxFibonacciSirasiFonksiyon5.Text = "" + rnd.Next(1, 20);
        }
        private void btnFibonacciDegeriFonsiyon5_Click(object sender, EventArgs e)
        {
            try
            {
                int fibonacciSirasi = Convert.ToInt32(txbxFibonacciSirasiFonksiyon5.Text);
                int x = 0, y = 1, z = 0;
                Console.Write("{0} {1}", x, y);

                for (int i = 2; i < fibonacciSirasi; i++)
                {
                    z = x + y;

                    Console.Write(" {0}", z);
                    x = y;
                    x = y;
                    y = z;
                }
                Console.Write(" \n");
                txbxFibonacciDegeriFonksiyon5.Text = "" + z;
            }
            catch (Exception)
            {
                MessageBox.Show("Hatalı Veri", "Lütfen Sayı Giriniz !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txbxFibonacciSirasiFonksiyon5.Text = "";
            }

        }
        private void btnTemizleFonksiyon5_Click(object sender, EventArgs e)
        {
            txbxFibonacciSirasiFonksiyon5.Text = "";
            txbxFibonacciDegeriFonksiyon5.Text = "";
        }
        #endregion

 

        
         
    }
}
